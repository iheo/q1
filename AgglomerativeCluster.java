import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AgglomerativeCluster {
	
	private int nData;				// number of data
	public int[] labels;			// labels, same number of elements as data
	private double[] dataMetric;	// one-time calculated distance matrix
	private int nMetrics;
	private int iLinkage;			// 0 - min, 1 - mean, 2 - max(furthest) when merging two points to one
	
	public AgglomerativeCluster(double[][] data) {
		super();
		this.nData = data.length;
		this.labels = new int[nData];
		this.nMetrics = cumsum(nData);
		this.iLinkage = 1;	// may be set different value later		
		computeMetric(data);		
	}	
	
	public void assignLabels(int nLabels) {
		int nClusters = nData, i, j, nmetric;
		int[] rowcol;
		
		/* clusterList is a 2-D arraylist.
		 *  As the size(clusterList) is decreasing, (by merging), 
		 *   each (merged) cluster has another arrayList for which data were belonged to itself
		 */ 
		List<List<Integer>> clusterList = new ArrayList<List<Integer>>();
		
		// elements are distance between cluster[i] and cluster[j]
		double[] clusterMetric;	
		
		// default
		for(i = 0; i < nData; i++) {	// default cluster is singleton(itself)
			clusterList.add(new ArrayList<Integer>());
			clusterList.get(i).add(i);
		}
				
		int iMin, k;
		
		while (nClusters > nLabels ) {	
			
			nmetric = cumsum(nClusters);
			clusterMetric = new double[nmetric];
			
			for(k = 0; k < nmetric; k++) {
				rowcol = ind2sub(k);	// rowcol is 2-d array where row = rowcol[0], col = rowcol[1];
				
				// compute metric for the current clusterList
				clusterMetric[k] = computeClusterMetric(clusterList.get(rowcol[0]), clusterList.get(rowcol[1]), iLinkage);
			}			
			
			iMin = getMinLoc(clusterMetric);
			rowcol = ind2sub(iMin);
			
			// update clusterList by merging the two points of minimum distance among all pairs
			clusterList = computeMergeClusters(clusterList, rowcol[0], rowcol[1]);
			nClusters = clusterList.size();
		}
		
		// once clusterList is updated until meeting the condition (nClusters == nLabels), 
		// store labels
		for(k = 0; k < nClusters; k++) {
			for(j = 0; j < clusterList.get(k).size(); j++ ) {
				i = clusterList.get(k).get(j);
				this.labels[i] = k;
			}		
		}
			 
	}
	
	private List<List<Integer>> computeMergeClusters(List<List<Integer>> clusterList, int i, int j) {		
		clusterList.get(i).addAll(clusterList.get(j));
		clusterList.remove(j);
		return clusterList;
	}
	
	private void computeMetric(double[][] data) {		
		this.dataMetric = new double[nMetrics];
		int[] rowcol;
		for(int k = 0; k < nMetrics; k++) {
			rowcol = ind2sub(k);
			dataMetric[k] = computeDataMetric(data[rowcol[0]], data[rowcol[1]]);
		}				
	}
	
	private double computeDataMetric(double[] x, double[] y) {
		double S = 0.0;
		for(int i = 0; i < x.length; i++)
			S += (x[i] - y[i])*(x[i] - y[i]);	// Euclidean distance for simplicity
		return S;
	}
	
	private double computeClusterMetric(List<Integer> list1, List<Integer> list2, int iLinkage) {
		
		int N = list1.size() * list2.size(), k = 0, j;
		double[] tmpMetric = new double[N];
		double metric = 0.0;
		
		for(int c1 = 0; c1 < list1.size(); c1++) {
			for(int c2 = 0; c2 < list2.size(); c2++) {
				try {
					j = sub2ind(list1.get(c1), list2.get(c2));
					tmpMetric[k++] = dataMetric[j];
				}
				catch (Exception e) {
					e.printStackTrace();
				}				
			}
		}
		
		switch(iLinkage) {
		case 0:	// 	minimum distance
			metric = getMin(tmpMetric);
			break;
		case 1:	// mean distance
			metric = getMean(tmpMetric);
			break;
		case 2:	// maximum distance (furthest)
			metric = getMax(tmpMetric);
			break;
		}		
		return metric;		
	}
	
	private int cumsum(int k) { 	
		// Cumulative sum up to k (from 0), cumsum(4) <- 0 + 1 + 2 + 3
		int S = 0;
		for(int i = 0; i < k; i++) 
			S += i;
		return S;
	}
	
	

	/*	indices to subindices(row, col)
	 * ind2sub(0) --- [0] and [1]
	 * ind2sub(1) --- [0] and [2]
	 * ind2sub(2) --- [1] and [2]
	 * ind2sub(3) --- [0] and [3]
	 * ind2sub(4) --- [1] and [3]
	 * ind2sub(5) --- [2] and [3]
	 * ind2sub(6) --- [0] and [4]
	 * ....
	 * 
	 */
	private int[] ind2sub(int index) {
		int[] rowcol = new int[2];
		rowcol[1] = (int) Math.round(Math.floor(-.5 + .5 * Math.sqrt(1 + 8*index)) + 1);
		rowcol[0] = Math.round((rowcol[1]+1)*(2-rowcol[1]) / 2 + index - 1);
		return rowcol;
	}
	
	
	/*	subindices to indices(i)
	 * sub2ind(0, 1) --- 0
	 * sub2ind(0, 2) --- 1
	 * sub2ind(1, 2) --- 2
	 * sub2ind(0, 3) --- 3
	 * sub2ind(1, 3) --- 4
	 * sub2ind(2, 3) --- 5
	 * sub2ind(0, 4) --- 6
	 * ....
	 * 
	 */
	private int sub2ind(int i, int j) {
		int tmp;
		if(i > j) {
			tmp = j; j = i; i = tmp;
		}
		return Math.round((j+1)*(j-2)/2 + i + 1);
	}
	
	
	// find the location of minimum point for an array
	private int getMinLoc(double[] arr) {
		double min = arr[0]; int imin = 0;
		for(int j = 1; j < arr.length; j++) {
			if(arr[j] < min) {
				imin = j;
				min = arr[j];
			}
		}
		return imin;
	}
	
	// get maximum value from an array
	private double getMax(double[] arr) {
		double max = arr[0];
		for(int i = 1; i < arr.length; i++) {
			if(arr[i] > max) {
				max = arr[i];
			}
		}
		return max;
	}
	
	// get mean value for an array
	private double getMean(double[] arr) {
		double mean = 0.0;
		for(int i = 0; i < arr.length; i++) {
			mean += arr[i];
		}
		return (mean/arr.length);
		
	}
	
	// get minimum value from an array
	private double getMin(double[] arr) {
		double min = arr[0];
		for(int i = 1; i < arr.length; i++) {
			if(arr[i] < min) {
				min = arr[i];
			}
		}
		return min;		
	}
	

}
