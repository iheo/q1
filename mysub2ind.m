function IND = sub2ind(I, J)

if ~all(I<J)
    tmp = I;
    I = J;
    J = tmp;
end

IND = round(J .* (J - 3) / 2 + I + 1);
