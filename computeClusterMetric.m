function d = computeClusterMetric(list1, list2, dataMetric, linkage)

k = 1; N = length(list1)*length(list2);
d_c = zeros(1, N);
for c1 = 1:length(list1)
    for c2 = 1:length(list2)
        ii = mysub2ind(list1(c1), list2(c2));
        try
            d_c(k) = dataMetric(ii);
        catch
            [list1(c1), list2(c2)]
            return;
        end
        k = k + 1;
    end
end

switch linkage
    case 0
        d = min(d_c);
    case 1
        d = mean(d_c);
    case 2
        d = max(d_c);
end