clear all;

nLabels = 4;
nlinkage = 1;
load fisheriris
data = meas';
[dim, ndata] = size(data);

fdist = @(x, y) sum((x - y).^2);    % same as dataMetric
%% compute initial large distance matrix

for k = 1:sum(1:ndata-1)
    [i, j] = myind2sub(k);
    dataMetric(k) = fdist(data(:, i), data(:, j));    % same as dataMetric
end

%% cluster
clusters = num2cell(1:ndata); % singleton cluster for each element
nclusters = length(clusters); % number of clusters

while nclusters > nLabels
    clusterMetric = zeros(sum(1:nclusters-1), 1);
    for k = 1 : sum(1:nclusters-1)
        [c1, c2] = myind2sub(k);
        clusterMetric(k) = computeClusterMetric(clusters{c1}, clusters{c2}, dataMetric, nlinkage);
        
    end
          
    [minval, ii] = min(clusterMetric(:));
    [i, j] = myind2sub(ii);
    
    clusters = merge_clusters(clusters, i, j);
    nclusters = length(clusters); % number of clusters
    
    fprintf('cluster merged to %d\n', nclusters);
end

labels = zeros(ndata, 1);
for i = 1:nclusters
    labels(clusters{i}) = i;
end

%% print labels
for i = 1:length(clusters)
    fprintf('Label %d - ', i)
    for j = 1:length(clusters{i})
        fprintf('%d, ', clusters{i}(j));
    end    
    fprintf('\n');
end