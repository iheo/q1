

function clusters = merge_clusters(clusters, i, j)
clusters{i} = [clusters{i}, clusters{j}];   % merge
clusters(j) = [];   % delete
